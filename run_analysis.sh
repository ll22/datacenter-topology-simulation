#!/bin/bash

echo "Start benchmark..."
#clean up dir
rm -rf analysis_results
mkdir analysis_results
cd analysis_results
mkdir data_rate
mkdir flow_pattern
cd ..

# topology to simuate
topology="Three-tier"
#topology="Fat-tree"
# data rate to test
data_rate_1="1Mbps"
data_rate_2="2Mbps"
data_rate_3="4Mbps"
data_rate_4="8Mbps"
data_rate_6="12Mbps"
data_rate_7="16Mbps"
data_rate_8="20Mbps"

data_rate_5="5Mbps"
#data_rate_4="70Mbps"

ns3_script="./src/flow-monitor/examples/flowmon-parse-results.py"

'''
echo "Test different data rate on random traffic"

fn="$topology-$data_rate_1"
fn+=".xml"
report_fn="$topology-$data_rate_1.txt"
./waf --run "scratch/$topology  analysis_results/data_rate/$fn  $data_rate_1 o"
python $ns3_script analysis_results/data_rate/$fn >> analysis_results/$report_fn

fn="$topology-$data_rate_2"
fn+=".xml"
report_fn="$topology-$data_rate_2.txt"
./waf --run "scratch/$topology  analysis_results/data_rate/$fn  $data_rate_2 o"
python $ns3_script analysis_results/data_rate/$fn >> analysis_results/$report_fn

fn="$topology-$data_rate_3"
fn+=".xml"
report_fn="$topology-$data_rate_3.txt"
./waf --run "scratch/$topology  analysis_results/data_rate/$fn  $data_rate_3 o"
python $ns3_script analysis_results/data_rate/$fn >> analysis_results/$report_fn
#python ./src/flow-monitor/examples/flowmon-parse-results.py analysis_results/data_rate/Three-tier-3Mbps.xml >> analysis_results/data_rate/Three-tier-1Mbps-overall.txt


fn="$topology-$data_rate_4"
fn+=".xml"
report_fn="$topology-$data_rate_4.txt"
./waf --run "scratch/$topology  analysis_results/data_rate/$fn  $data_rate_4 o"
python $ns3_script analysis_results/data_rate/$fn >> analysis_results/$report_fn
#python ./src/flow-monitor/examples/flowmon-parse-results.py analysis_results/data_rate/Three-tier-3Mbps.xml >> analysis_results/data_rate/Three-tier-1Mbps-overall.txt


fn="$topology-$data_rate_6"
fn+=".xml"
report_fn="$topology-$data_rate_6.txt"
./waf --run "scratch/$topology  analysis_results/data_rate/$fn  $data_rate_6 o"
python $ns3_script analysis_results/data_rate/$fn >> analysis_results/$report_fn
#python ./src/flow-monitor/examples/flowmon-parse-results.py analysis_results/data_rate/Three-tier-3Mbps.xml >> analysis_results/data_rate/Three-tier-1Mbps-overall.txt


fn="$topology-$data_rate_7"
fn+=".xml"
report_fn="$topology-$data_rate_7.txt"
./waf --run "scratch/$topology  analysis_results/data_rate/$fn  $data_rate_7 o"
python $ns3_script analysis_results/data_rate/$fn >> analysis_results/$report_fn
#python ./src/flow-monitor/examples/flowmon-parse-results.py analysis_results/data_rate/Three-tier-3Mbps.xml >> analysis_results/data_rate/Three-tier-1Mbps-overall.txt


fn="$topology-$data_rate_8"
fn+=".xml"
report_fn="$topology-$data_rate_8.txt"
./waf --run "scratch/$topology  analysis_results/data_rate/$fn  $data_rate_8 o"
python $ns3_script analysis_results/data_rate/$fn >> analysis_results/$report_fn
#python ./src/flow-monitor/examples/flowmon-parse-results.py analysis_results/data_rate/Three-tier-3Mbps.xml >> analysis_results/data_rate/Three-tier-1Mbps-overall.txt

'''

'''
echo "Test same data rate on different traffic patterns"
fn="$topology-$data_rate_5-"
fn+="overall"
fn+=".xml"
report_fn="$topology-overall-$data_rate_5.txt"
./waf --run "scratch/$topology  analysis_results/flow_pattern/$fn  $data_rate_5 o"
python $ns3_script analysis_results/flow_pattern/$fn >> analysis_results/$report_fn

fn="$topology-$data_rate_5-"
fn+="local"
fn+=".xml"
report_fn="$topology-local-$data_rate_5.txt"
./waf --run "scratch/$topology  analysis_results/flow_pattern/$fn  $data_rate_5 l"
python $ns3_script analysis_results/flow_pattern/$fn >> analysis_results/$report_fn

fn="$topology-$data_rate_5-"
fn+="core"
fn+=".xml"
report_fn="$topology-core-$data_rate_5.txt"
./waf --run "scratch/$topology  analysis_results/flow_pattern/$fn  $data_rate_5 c"
python $ns3_script analysis_results/flow_pattern/$fn >> analysis_results/$report_fn
'''

'''
#different packet size
fn="$topology-$data_rate_5-"
fn+="256BytesPacket"
fn+=".xml"
report_fn="$topology-256BytesPacket-$data_rate_5.txt"
./waf --run "scratch/$topology  analysis_results/flow_pattern/$fn  $data_rate_5 o 2"
python $ns3_script analysis_results/flow_pattern/$fn >> analysis_results/$report_fn



fn="$topology-$data_rate_5-"
fn+="512BytesPacket"
fn+=".xml"
report_fn="$topology-512BytesPacket-$data_rate_5.txt"
./waf --run "scratch/$topology  analysis_results/flow_pattern/$fn  $data_rate_5 o 5"
python $ns3_script analysis_results/flow_pattern/$fn >> analysis_results/$report_fn



fn="$topology-$data_rate_5-"
fn+="1024BytesPacket"
fn+=".xml"
report_fn="$topology-1024BytesPacket-$data_rate_5.txt"
./waf --run "scratch/$topology  analysis_results/flow_pattern/$fn  $data_rate_5 o"
python $ns3_script analysis_results/flow_pattern/$fn >> analysis_results/$report_fn
'''
'''
#different packet size
fn="$topology-$data_rate_5-"
fn+="256BytesPacket"
fn+=".xml"
report_fn="$topology-8host-$data_rate_5.txt"
./waf --run "scratch/$topology  analysis_results/flow_pattern/$fn  $data_rate_5 o 1 8"
python $ns3_script analysis_results/flow_pattern/$fn >> analysis_results/$report_fn



fn="$topology-$data_rate_5-"
fn+="512BytesPacket"
fn+=".xml"
report_fn="$topology-16host-$data_rate_5.txt"
./waf --run "scratch/$topology  analysis_results/flow_pattern/$fn  $data_rate_5 o 1 16"
python $ns3_script analysis_results/flow_pattern/$fn >> analysis_results/$report_fn



fn="$topology-$data_rate_5-"
fn+="1024BytesPacket"
fn+=".xml"
report_fn="$topology-32host-$data_rate_5.txt"
./waf --run "scratch/$topology  analysis_results/flow_pattern/$fn  $data_rate_5 o 1 32"
python $ns3_script analysis_results/flow_pattern/$fn >> analysis_results/$report_fn


fn="$topology-$data_rate_5-"
fn+="1024BytesPacket"
fn+=".xml"
report_fn="$topology-64host-$data_rate_5.txt"
./waf --run "scratch/$topology  analysis_results/flow_pattern/$fn  $data_rate_5 o 1 64"
python $ns3_script analysis_results/flow_pattern/$fn >> analysis_results/$report_fn
'''


echo "Test different 128 hosts"
fn="$topology-$data_rate_5-"
fn+="1024BytesPacket"
fn+=".xml"
report_fn="$topology-128host-$data_rate_5.txt"
./waf --run "scratch/$topology  analysis_results/flow_pattern/$fn  $data_rate_5 c 1"
python $ns3_script analysis_results/flow_pattern/$fn >> analysis_results/$report_fn

python csv_generator.py
