#!/usr/bin/python
'''
Read rx/tx rate data from .txt files and write to csv
'''
import os
import csv

files = os.listdir('./analysis_results')
print files
topology = 'Three-tier'
txBitrate = 'TX bitrate: '
rxBitrate = 'RX bitrate: '
#RESULT = ['apple','cherry','orange','pineapple','strawberry']
writer = csv.writer(open('./analysis_results/'+topology+'-report.csv', 'w'), dialect='excel')

for eachFile in files:
	if '.txt' not in eachFile:
		continue
	writer.writerow([''])
	writer.writerow([eachFile]) #write filename to the csv file

	filename = './analysis_results/'+eachFile
	fd = open(filename, 'r')
	lines = fd.readlines()
	txRates = ['txRates(kbit/s)']
	rxRates = ['rxRates(kbit/s)']
	for eachLine in lines:

		if txBitrate in eachLine:
			rate = eachLine[eachLine.index(txBitrate) + len(txBitrate):].split(' ')[0]
			#print 'rate is: '+rate
			txRates.append(rate)

		elif rxBitrate in eachLine:
			rate = eachLine[eachLine.index(rxBitrate) + len(rxBitrate):].split(' ')[0]
			#print 'rate is: '+rate
			rxRates.append(rate)
		#writer.writerow(RESULT)
	writer.writerow(rxRates)
	writer.writerow(txRates)
#writer.writerow(RESULT)


